/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author MarkII
 */
@Entity
@Table(name = "almacen_has_presentacion")
@NamedQueries({
    @NamedQuery(name = "AlmacenHasPresentacion.findAll", query = "SELECT a FROM AlmacenHasPresentacion a"),
    @NamedQuery(name = "AlmacenHasPresentacion.findByIdRegistroAlmacen", query = "SELECT a FROM AlmacenHasPresentacion a WHERE a.idRegistroAlmacen = :idRegistroAlmacen"),
    @NamedQuery(name = "AlmacenHasPresentacion.findByStock", query = "SELECT a FROM AlmacenHasPresentacion a WHERE a.stock = :stock"),
    @NamedQuery(name = "AlmacenHasPresentacion.findByPrecioCompra", query = "SELECT a FROM AlmacenHasPresentacion a WHERE a.precioCompra = :precioCompra"),
    @NamedQuery(name = "AlmacenHasPresentacion.findByPrecioVenta", query = "SELECT a FROM AlmacenHasPresentacion a WHERE a.precioVenta = :precioVenta")})
public class AlmacenHasPresentacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    //@NotNull
    @Column(name = "idRegistroAlmacen")
    private Integer idRegistroAlmacen;
    @Basic(optional = false)
    @NotNull
    @Column(name = "stock")
    private long stock;
    @Basic(optional = false)
    @NotNull
    @Column(name = "precioCompra")
    private long precioCompra;
    @Basic(optional = false)
    @NotNull
    @Column(name = "precioVenta")
    private long precioVenta;
    @JoinColumn(name = "Almacen_idProducto", referencedColumnName = "idProducto")
    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    private Almacen almacenidProducto;
    @JoinColumn(name = "presentacion_idPresentacion", referencedColumnName = "idPresentacion")
    @ManyToOne(optional = false)
    private Presentacion presentacionidPresentacion;

    public AlmacenHasPresentacion() {
    }

    public AlmacenHasPresentacion(Integer idRegistroAlmacen) {
        this.idRegistroAlmacen = idRegistroAlmacen;
    }

    public AlmacenHasPresentacion(Integer idRegistroAlmacen, long stock, long precioCompra, long precioVenta) {
        this.idRegistroAlmacen = idRegistroAlmacen;
        this.stock = stock;
        this.precioCompra = precioCompra;
        this.precioVenta = precioVenta;
    }
    

    public Integer getIdRegistroAlmacen() {
        return idRegistroAlmacen;
    }

    public void setIdRegistroAlmacen(Integer idRegistroAlmacen) {
        this.idRegistroAlmacen = idRegistroAlmacen;
    }

    public long getStock() {
        return stock;
    }

    public void setStock(long stock) {
        this.stock = stock;
    }

    public long getPrecioCompra() {
        return precioCompra;
    }

    public void setPrecioCompra(long precioCompra) {
        this.precioCompra = precioCompra;
    }

    public long getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(long precioVenta) {
        this.precioVenta = precioVenta;
    }

    public Almacen getAlmacenidProducto() {
        return almacenidProducto;
    }

    public void setAlmacenidProducto(Almacen almacenidProducto) {
        this.almacenidProducto = almacenidProducto;
    }

    public Presentacion getPresentacionidPresentacion() {
        return presentacionidPresentacion;
    }

    public void setPresentacionidPresentacion(Presentacion presentacionidPresentacion) {
        this.presentacionidPresentacion = presentacionidPresentacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRegistroAlmacen != null ? idRegistroAlmacen.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AlmacenHasPresentacion)) {
            return false;
        }
        AlmacenHasPresentacion other = (AlmacenHasPresentacion) object;
        if ((this.idRegistroAlmacen == null && other.idRegistroAlmacen != null) || (this.idRegistroAlmacen != null && !this.idRegistroAlmacen.equals(other.idRegistroAlmacen))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.AlmacenHasPresentacion[ idRegistroAlmacen=" + idRegistroAlmacen + " ]";
    }
    
}
