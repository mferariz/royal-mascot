/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author MarkII
 */
@Entity
@Table(name = "almacen")
@NamedQueries({
    @NamedQuery(name = "Almacen.findAll", query = "SELECT a FROM Almacen a"),
    @NamedQuery(name = "Almacen.findByIdProducto", query = "SELECT a FROM Almacen a WHERE a.idProducto = :idProducto"),
    @NamedQuery(name = "Almacen.findByFechaIngreso", query = "SELECT a FROM Almacen a WHERE a.fechaIngreso = :fechaIngreso"),
    @NamedQuery(name = "Almacen.findByStatus", query = "SELECT a FROM Almacen a WHERE a.status = :status"),
    @NamedQuery(name = "Almacen.findByNombre", query = "SELECT a FROM Almacen a WHERE a.nombre = :nombre"),
    @NamedQuery(name = "Almacen.findByDescripcion", query = "SELECT a FROM Almacen a WHERE a.descripcion = :descripcion"),
    @NamedQuery(name = "Almacen.findByMarca", query = "SELECT a FROM Almacen a WHERE a.marca = :marca")})
public class Almacen implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idProducto")
    private Integer idProducto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fechaIngreso")
    @Temporal(TemporalType.DATE)
    private Date fechaIngreso;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "status")
    private String status;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "marca")
    private String marca;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "almacenidProducto")
    private List<Merma> mermaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "almacenidProducto")
    private List<Promocion> promocionList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "almacen")
    private List<VentaHasAlmacen> ventaHasAlmacenList;
    @JoinColumn(name = "Proveedor_idProveedor", referencedColumnName = "idProveedor")
    @ManyToOne(optional = false)
    private Proveedor proveedoridProveedor;
    @JoinColumn(name = "TipoProducto_idProducto", referencedColumnName = "idTipoProducto")
    @ManyToOne(optional = false)
    private Tipoproducto tipoProductoidProducto;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "almacenidProducto")
    private List<AlmacenHasPresentacion> almacenHasPresentacionList;

    public Almacen() {
    }

    public Almacen(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Almacen(Integer idProducto, Date fechaIngreso, String status, String nombre, String descripcion, String marca) {
        this.idProducto = idProducto;
        this.fechaIngreso = fechaIngreso;
        this.status = status;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.marca = marca;
    }

    public void agregarProductoAlmacen(AlmacenHasPresentacion producto){
        if(this.almacenHasPresentacionList == null){
            this.almacenHasPresentacionList = new ArrayList<>();
        }
        
        this.almacenHasPresentacionList.add(producto);
    }

    
    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public List<Merma> getMermaList() {
        return mermaList;
    }

    public void setMermaList(List<Merma> mermaList) {
        this.mermaList = mermaList;
    }

    public List<Promocion> getPromocionList() {
        return promocionList;
    }

    public void setPromocionList(List<Promocion> promocionList) {
        this.promocionList = promocionList;
    }

    public List<VentaHasAlmacen> getVentaHasAlmacenList() {
        return ventaHasAlmacenList;
    }

    public void setVentaHasAlmacenList(List<VentaHasAlmacen> ventaHasAlmacenList) {
        this.ventaHasAlmacenList = ventaHasAlmacenList;
    }

    public Proveedor getProveedoridProveedor() {
        return proveedoridProveedor;
    }

    public void setProveedoridProveedor(Proveedor proveedoridProveedor) {
        this.proveedoridProveedor = proveedoridProveedor;
    }

    public Tipoproducto getTipoProductoidProducto() {
        return tipoProductoidProducto;
    }

    public void setTipoProductoidProducto(Tipoproducto tipoProductoidProducto) {
        this.tipoProductoidProducto = tipoProductoidProducto;
    }

    public List<AlmacenHasPresentacion> getAlmacenHasPresentacionList() {
        return almacenHasPresentacionList;
    }

    public void setAlmacenHasPresentacionList(List<AlmacenHasPresentacion> almacenHasPresentacionList) {
        this.almacenHasPresentacionList = almacenHasPresentacionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProducto != null ? idProducto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Almacen)) {
            return false;
        }
        Almacen other = (Almacen) object;
        if ((this.idProducto == null && other.idProducto != null) || (this.idProducto != null && !this.idProducto.equals(other.idProducto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.Almacen[ idProducto=" + idProducto + " ]";
    }
    
}
