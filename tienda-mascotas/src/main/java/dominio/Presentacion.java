/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author MarkII
 */
@Entity
@Table(name = "presentacion")
@NamedQueries({
    @NamedQuery(name = "Presentacion.findAll", query = "SELECT p FROM Presentacion p"),
    @NamedQuery(name = "Presentacion.findByIdPresentacion", query = "SELECT p FROM Presentacion p WHERE p.idPresentacion = :idPresentacion"),
    @NamedQuery(name = "Presentacion.findByNombrePre", query = "SELECT p FROM Presentacion p WHERE p.nombrePre = :nombrePre"),
    @NamedQuery(name = "Presentacion.findByDescripcionPre", query = "SELECT p FROM Presentacion p WHERE p.descripcionPre = :descripcionPre")})
public class Presentacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idPresentacion")
    private Integer idPresentacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nombrePre")
    private String nombrePre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "descripcionPre")
    private String descripcionPre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "presentacionidPresentacion")
    private List<AlmacenHasPresentacion> almacenHasPresentacionList;

    public Presentacion() {
    }

    public Presentacion(Integer idPresentacion) {
        this.idPresentacion = idPresentacion;
    }

    public Presentacion(Integer idPresentacion, String nombrePre, String descripcionPre) {
        this.idPresentacion = idPresentacion;
        this.nombrePre = nombrePre;
        this.descripcionPre = descripcionPre;
    }

    public Integer getIdPresentacion() {
        return idPresentacion;
    }

    public void setIdPresentacion(Integer idPresentacion) {
        this.idPresentacion = idPresentacion;
    }

    public String getNombrePre() {
        return nombrePre;
    }

    public void setNombrePre(String nombrePre) {
        this.nombrePre = nombrePre;
    }

    public String getDescripcionPre() {
        return descripcionPre;
    }

    public void setDescripcionPre(String descripcionPre) {
        this.descripcionPre = descripcionPre;
    }

    public List<AlmacenHasPresentacion> getAlmacenHasPresentacionList() {
        return almacenHasPresentacionList;
    }

    public void setAlmacenHasPresentacionList(List<AlmacenHasPresentacion> almacenHasPresentacionList) {
        this.almacenHasPresentacionList = almacenHasPresentacionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPresentacion != null ? idPresentacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Presentacion)) {
            return false;
        }
        Presentacion other = (Presentacion) object;
        if ((this.idPresentacion == null && other.idPresentacion != null) || (this.idPresentacion != null && !this.idPresentacion.equals(other.idPresentacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.Presentacion[ idPresentacion=" + idPresentacion + " ]";
    }
    
}
