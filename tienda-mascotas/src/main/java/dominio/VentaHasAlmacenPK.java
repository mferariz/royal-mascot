/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author MarkII
 */
@Embeddable
public class VentaHasAlmacenPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "Venta_idVenta")
    private int ventaidVenta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Almacen_idProducto")
    private int almacenidProducto;

    public VentaHasAlmacenPK() {
    }

    public VentaHasAlmacenPK(int ventaidVenta, int almacenidProducto) {
        this.ventaidVenta = ventaidVenta;
        this.almacenidProducto = almacenidProducto;
    }

    public int getVentaidVenta() {
        return ventaidVenta;
    }

    public void setVentaidVenta(int ventaidVenta) {
        this.ventaidVenta = ventaidVenta;
    }

    public int getAlmacenidProducto() {
        return almacenidProducto;
    }

    public void setAlmacenidProducto(int almacenidProducto) {
        this.almacenidProducto = almacenidProducto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) ventaidVenta;
        hash += (int) almacenidProducto;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VentaHasAlmacenPK)) {
            return false;
        }
        VentaHasAlmacenPK other = (VentaHasAlmacenPK) object;
        if (this.ventaidVenta != other.ventaidVenta) {
            return false;
        }
        if (this.almacenidProducto != other.almacenidProducto) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.VentaHasAlmacenPK[ ventaidVenta=" + ventaidVenta + ", almacenidProducto=" + almacenidProducto + " ]";
    }
    
}
