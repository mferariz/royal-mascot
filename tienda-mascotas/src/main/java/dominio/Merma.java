/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author MarkII
 */
@Entity
@Table(name = "merma")
@NamedQueries({
    @NamedQuery(name = "Merma.findAll", query = "SELECT m FROM Merma m"),
    @NamedQuery(name = "Merma.findByIdMerma", query = "SELECT m FROM Merma m WHERE m.idMerma = :idMerma"),
    @NamedQuery(name = "Merma.findByFechaIngreso", query = "SELECT m FROM Merma m WHERE m.fechaIngreso = :fechaIngreso"),
    @NamedQuery(name = "Merma.findByMotivo", query = "SELECT m FROM Merma m WHERE m.motivo = :motivo"),
    @NamedQuery(name = "Merma.findByCantidad", query = "SELECT m FROM Merma m WHERE m.cantidad = :cantidad")})
public class Merma implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idMerma")
    private Integer idMerma;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fechaIngreso")
    @Temporal(TemporalType.DATE)
    private Date fechaIngreso;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "motivo")
    private String motivo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cantidad")
    private long cantidad;
    @JoinColumn(name = "Almacen_idProducto", referencedColumnName = "idProducto")
    @ManyToOne(optional = false)
    private Almacen almacenidProducto;

    public Merma() {
    }

    public Merma(Integer idMerma) {
        this.idMerma = idMerma;
    }

    public Merma(Integer idMerma, Date fechaIngreso, String motivo, long cantidad) {
        this.idMerma = idMerma;
        this.fechaIngreso = fechaIngreso;
        this.motivo = motivo;
        this.cantidad = cantidad;
    }

    public Integer getIdMerma() {
        return idMerma;
    }

    public void setIdMerma(Integer idMerma) {
        this.idMerma = idMerma;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public long getCantidad() {
        return cantidad;
    }

    public void setCantidad(long cantidad) {
        this.cantidad = cantidad;
    }

    public Almacen getAlmacenidProducto() {
        return almacenidProducto;
    }

    public void setAlmacenidProducto(Almacen almacenidProducto) {
        this.almacenidProducto = almacenidProducto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMerma != null ? idMerma.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Merma)) {
            return false;
        }
        Merma other = (Merma) object;
        if ((this.idMerma == null && other.idMerma != null) || (this.idMerma != null && !this.idMerma.equals(other.idMerma))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.Merma[ idMerma=" + idMerma + " ]";
    }
    
}
