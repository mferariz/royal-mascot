/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dominio;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author MarkII
 */
@Entity
@Table(name = "venta_has_almacen")
@NamedQueries({
    @NamedQuery(name = "VentaHasAlmacen.findAll", query = "SELECT v FROM VentaHasAlmacen v"),
    @NamedQuery(name = "VentaHasAlmacen.findByVentaidVenta", query = "SELECT v FROM VentaHasAlmacen v WHERE v.ventaHasAlmacenPK.ventaidVenta = :ventaidVenta"),
    @NamedQuery(name = "VentaHasAlmacen.findByAlmacenidProducto", query = "SELECT v FROM VentaHasAlmacen v WHERE v.ventaHasAlmacenPK.almacenidProducto = :almacenidProducto"),
    @NamedQuery(name = "VentaHasAlmacen.findByCantidad", query = "SELECT v FROM VentaHasAlmacen v WHERE v.cantidad = :cantidad"),
    @NamedQuery(name = "VentaHasAlmacen.findByPrecioUnitario", query = "SELECT v FROM VentaHasAlmacen v WHERE v.precioUnitario = :precioUnitario")})
public class VentaHasAlmacen implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected VentaHasAlmacenPK ventaHasAlmacenPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cantidad")
    private long cantidad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "precioUnitario")
    private long precioUnitario;
    @JoinColumn(name = "Almacen_idProducto", referencedColumnName = "idProducto", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Almacen almacen;
    @JoinColumn(name = "Promocion_idPromocion", referencedColumnName = "idPromocion")
    @ManyToOne
    private Promocion promocionidPromocion;
    @JoinColumn(name = "Venta_idVenta", referencedColumnName = "idVenta", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Venta venta;

    public VentaHasAlmacen() {
    }

    public VentaHasAlmacen(VentaHasAlmacenPK ventaHasAlmacenPK) {
        this.ventaHasAlmacenPK = ventaHasAlmacenPK;
    }

    public VentaHasAlmacen(VentaHasAlmacenPK ventaHasAlmacenPK, long cantidad, long precioUnitario) {
        this.ventaHasAlmacenPK = ventaHasAlmacenPK;
        this.cantidad = cantidad;
        this.precioUnitario = precioUnitario;
    }

    public VentaHasAlmacen(int ventaidVenta, int almacenidProducto) {
        this.ventaHasAlmacenPK = new VentaHasAlmacenPK(ventaidVenta, almacenidProducto);
    }

    public VentaHasAlmacenPK getVentaHasAlmacenPK() {
        return ventaHasAlmacenPK;
    }

    public void setVentaHasAlmacenPK(VentaHasAlmacenPK ventaHasAlmacenPK) {
        this.ventaHasAlmacenPK = ventaHasAlmacenPK;
    }

    public long getCantidad() {
        return cantidad;
    }

    public void setCantidad(long cantidad) {
        this.cantidad = cantidad;
    }

    public long getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(long precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public Almacen getAlmacen() {
        return almacen;
    }

    public void setAlmacen(Almacen almacen) {
        this.almacen = almacen;
    }

    public Promocion getPromocionidPromocion() {
        return promocionidPromocion;
    }

    public void setPromocionidPromocion(Promocion promocionidPromocion) {
        this.promocionidPromocion = promocionidPromocion;
    }

    public Venta getVenta() {
        return venta;
    }

    public void setVenta(Venta venta) {
        this.venta = venta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ventaHasAlmacenPK != null ? ventaHasAlmacenPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VentaHasAlmacen)) {
            return false;
        }
        VentaHasAlmacen other = (VentaHasAlmacen) object;
        if ((this.ventaHasAlmacenPK == null && other.ventaHasAlmacenPK != null) || (this.ventaHasAlmacenPK != null && !this.ventaHasAlmacenPK.equals(other.ventaHasAlmacenPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "dominio.VentaHasAlmacen[ ventaHasAlmacenPK=" + ventaHasAlmacenPK + " ]";
    }
    
}
