package converter;

import dominio.Proveedor;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author HOME
 */
//@FacesConverter(value="GroupsConverter")
@FacesConverter(forClass=Proveedor.class, value = "pConverter")
public class converterProveedor implements Converter {

    public static Proveedor provs;

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if(provs == null){
            provs = new Proveedor(Integer.valueOf(value));
            return provs;
        }else
            throw new ConverterException(new FacesMessage(String.format("Cannot convert %s to Groups", value)));
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return String.valueOf(((Proveedor) value).getIdProveedor());
    }

}