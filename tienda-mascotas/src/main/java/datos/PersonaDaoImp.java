/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import dominio.Persona;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.*;

/**
 *
 * @author MarkII
 */
@Stateless
public class PersonaDaoImp implements PersonaDao{
    
    @PersistenceContext(unitName="tiendaMascotasPU")
    EntityManager em;

    @Override
    public List<Persona> enconrtarTodasPersonas() {
       return em.createNamedQuery("Persona.findAll").getResultList();
    }

    @Override
    public Persona encontrarPersonaPorId(Persona persona) {
        return em.find(Persona.class, persona.getIdPersona());
    }

    @Override
    public Persona encontrarPersonaPorCorreo(Persona persona) {
        Query query = em.createQuery("from persona p where p.correo =: correo");
        query.setParameter("correo", persona.getCorreo());
        return (Persona) query.getSingleResult();
    }
    
    @Override
    public Persona encontrarPersonaPorApellidos(Persona persona){
        return em.find(Persona.class, persona.getApellidos());
    }

    @Override
    public void modificarPersona(Persona persona) {
        em.merge(persona);
    }

    @Override
    public void insertarPersona(Persona persona) {
        em.persist(persona);
    }

    
    //metodo exlcuido de su uso por requerimiento de cliente
    @Override
    public void eliminarPersona(Persona persona){
        em.remove(em.merge(persona));
    }
    
}
