/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import dominio.Vendedor;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author MarkII
 */
@Stateless
public class VendedorDaoImp implements VendedorDao{
    
    @PersistenceContext(unitName = "tiendaMascotasPU")
    EntityManager em;

    @Override
    public List<Vendedor> listarVendedores() {
        return em.createNamedQuery("Vendedor.findAll").getResultList();
    }

    @Override
    public Vendedor buscarPorId(Vendedor vendedor) {
        return em.find(Vendedor.class, vendedor.getIdVendedor());
    }

    @Override
    public void crearVendedor(Vendedor vendedor) {
        em.persist(vendedor);
    }

    @Override
    public void modificarVendedor(Vendedor vendedor) {
        em.merge(vendedor);
    }
    
}
