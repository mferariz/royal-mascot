/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import dominio.Cliente;
import dominio.Mascota;
import java.util.List;

/**
 *
 * @author MarkII
 */
public interface clienteMascotaDao {
    //Cliente
    public List<Cliente> listadoClientes();
    public void crearCliente(Cliente cliente);
    public void modificarCliente(Cliente cliente);
    //Mascota
    public List<Mascota> listadoMascotas();
    public void crearMascota(Mascota mascota);
    public void modificarMascota(Mascota mascota);
}
