/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import dominio.Gasto;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.*;

/**
 *
 * @author luis-
 */
@Stateless
public class GastoDaoImp implements GastoDao{

    @PersistenceContext(unitName="tiendaMascotasPU")
    EntityManager em;
    
    @Override
    public List<Gasto> encontrarGVF(Gasto gasto) {        
        Query query = em.createQuery("SELECT g FROM Gasto g WHERE g.tipo = :tipo and g.fecha = :fecha");
        query.setParameter("tipo", "Variable");
        query.setParameter("fecha", gasto.getFecha());
        return query.getResultList(); 
    }
    
    @Override
    public List<Gasto> encontrarGPF(Date fechaI, Date fechaF){
        Query query = em.createQuery("SELECT g FROM Gasto g WHERE g.tipo = :tipo and g.fecha BETWEEN :fechaI and :fechaF");
        query.setParameter("tipo", "Permanente");
        query.setParameter("fechaI", fechaI);
        query.setParameter("fechaF", fechaF);
        return query.getResultList(); 
    }
    
    @Override
    public List<Gasto> encontrarGT(Gasto gasto) {
        Query query = em.createQuery("SELECT g FROM Gasto g WHERE g.tipo = :tipo");
        query.setParameter("tipo", gasto.getTipo());
        return query.getResultList();                
    }
    
    @Override
    public void modificarGasto(Gasto gasto) {
        em.merge(gasto);
    }

    @Override
    public void insertarGasto(Gasto gasto) {
        em.persist(gasto);
    }

    @Override
    public void eliminarGasto(Gasto gasto) {
        em.remove(gasto);
    }

}
