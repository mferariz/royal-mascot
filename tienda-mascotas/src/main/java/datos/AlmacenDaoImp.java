/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import dominio.Almacen;
import dominio.AlmacenHasPresentacion;
import dominio.Cliente;
import dominio.Mascota;
import dominio.Presentacion;
import dominio.Proveedor;
import dominio.Tipoproducto;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author MarkII
 */
@Stateless
public class AlmacenDaoImp implements AlmacenDao {

    @PersistenceContext(unitName = "tiendaMascotasPU")
    EntityManager em;

    @Override
    public List<AlmacenHasPresentacion> listarProductos() {
        return em.createNamedQuery("AlmacenHasPresentacion.findAll").getResultList();
    }

    @Override
    public void agregarproducto(AlmacenHasPresentacion almacenHasPresentacion) {
        try {
            em.persist(almacenHasPresentacion);
        } catch (ConstraintViolationException e) {
            // Aqui tira los errores de constraint
            e.getConstraintViolations().forEach(actual -> {
                System.out.println("EXCEPCION ENCONTRADA. " + actual.toString());
            });
        }

    }
    
    @Override
    public void modificarProducto (AlmacenHasPresentacion productoModificar){
        try {
            em.merge(productoModificar);
        } catch (ConstraintViolationException e) {
            // Aqui tira los errores de constraint
            e.getConstraintViolations().forEach(actual -> {
                System.out.println("EXCEPCION ENCONTRADA. " + actual.toString());
            });
        }
    }

    @Override
    public List<Proveedor> listarProveedores() {
        return em.createNamedQuery("Proveedor.findAll").getResultList();
    }

    @Override
    public List<Tipoproducto> listarTipoProducto() {
        return em.createNamedQuery("Tipoproducto.findAll").getResultList();
    }

    @Override
    public List<Presentacion> listarPresentacion() {
        return em.createNamedQuery("Presentacion.findAll").getResultList();
    }
    
    @Override
    public List<Almacen> listarAlmacer(){
        return em.createNamedQuery("Almacen.findAll").getResultList();
    }
    

}
