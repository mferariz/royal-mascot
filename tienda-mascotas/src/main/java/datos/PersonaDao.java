/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import dominio.Persona;
import java.util.List;

/**
 *
 * @author MarkII
 */
public interface PersonaDao {
    public List<Persona> enconrtarTodasPersonas();
    public Persona encontrarPersonaPorId(Persona persona);
    public Persona encontrarPersonaPorCorreo(Persona persona);
    public Persona encontrarPersonaPorApellidos(Persona persona);
    public void modificarPersona(Persona persona);
    public void insertarPersona(Persona persona);
    public void eliminarPersona(Persona persona);
}
