/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos.imp;

import datos.PromocionDao;
import dominio.Promocion;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author m_fer
 */
@Stateless
public class PromocionDaoImp implements PromocionDao{
    
    @PersistenceContext(unitName = "tiendaMascotasPU")
    EntityManager em;

    @Override
    public List<Promocion> traeTodo() {
        return em.createNamedQuery("Promocion.findAll").getResultList();
    }

    @Override
    public Promocion traePorId(Integer id) {
        return em.find(Promocion.class, id);
    }

    @Override
    public void insertar(Promocion o) {
        em.persist(o);
    }

    @Override
    public void modificar(Promocion o) {
        em.merge(o);
    }

    @Override
    public void eliminar(Promocion o) {
        em.remove(o);
    }
    
}
