/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos.imp;

import datos.ProveedorDao;
import dominio.Proveedor;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author m_fer
 */
@Stateless
public class ProveedorDaoImp implements ProveedorDao{
    
    @PersistenceContext(unitName="tiendaMascotasPU")
    EntityManager em;

    @Override
    public List<Proveedor> traeTodo() {
        return em.createNamedQuery("Proveedor.findAll").getResultList();
    }

    @Override
    public Proveedor traePorId(Integer id) {
        return em.find(Proveedor.class, id);
    }

    @Override
    public void insertar(Proveedor o) {
        em.persist(o);
    }

    @Override
    public void modificar(Proveedor o) {
        em.merge(o);
    }

    @Override
    public void eliminar(Proveedor o) {
        em.remove(o);
    }
    
}
