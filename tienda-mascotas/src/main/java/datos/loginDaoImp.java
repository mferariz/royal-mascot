/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import dominio.Administrador;
import dominio.Vendedor;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.ConstraintViolationException;

/**
 *
 * @author MarkII
 */
@Stateless
public class loginDaoImp implements loginDao {

    @PersistenceContext(unitName = "tiendaMascotasPU")
    private EntityManager em;

    @Override
    public Administrador accederAdministrador(Administrador admin) {
        Administrador adminRec;
        String correoRec, correoControl;
        String passRec, pasControl;
        try {
            adminRec = (Administrador) em.createNamedQuery("Administrador.findByContrasenaA").getSingleResult();
            Query query = em.createQuery("from Administrador a where a.contrasena_a =: pass");
            query.setParameter("pass", admin.getContrasenaA());
            correoRec = adminRec.getPersonaidPersona().getCorreo();
            passRec = adminRec.getContrasenaA();
            correoControl = admin.getPersonaidPersona().getCorreo();
            pasControl = admin.getContrasenaA();

            if (correoControl.equals(correoRec) && pasControl.equals(passRec)) {
                return adminRec;
            } else {
                return null;
            }

        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public Vendedor accederVendedor(Vendedor vendedor) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
