package datos;

import java.util.List;

/**
 *
 * @author m_fer
 */
public interface Dao<Object> {

    public List<Object> traeTodo();
    public Object traePorId(Integer id);
    public void insertar(Object o);
    public void modificar(Object o);
    public void eliminar(Object o);    

}
