/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import dominio.Administrador;
import dominio.Vendedor;
import java.util.List;

/**
 *
 * @author MarkII
 */
public interface loginDao {
    public Administrador accederAdministrador(Administrador admin);
    public Vendedor accederVendedor(Vendedor vendedor);
}
