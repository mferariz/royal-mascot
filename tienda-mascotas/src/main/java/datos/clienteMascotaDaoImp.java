/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import dominio.Cliente;
import dominio.Mascota;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author MarkII
 */
@Stateless
public class clienteMascotaDaoImp implements clienteMascotaDao{

    @PersistenceContext(unitName = "tiendaMascotasPU")
    EntityManager em;
    
    //Cliente
    @Override
    public List<Cliente> listadoClientes() {
        return em.createNamedQuery("Cliente.findAll").getResultList();
    }

    @Override
    public void crearCliente(Cliente cliente) {
        em.persist(cliente);
    }

    @Override
    public void modificarCliente(Cliente cliente) {
        em.merge(cliente);
    }

    
    //Mascota
    @Override
    public List<Mascota> listadoMascotas() {
        return em.createNamedQuery("Mascota.findAll").getResultList();
    }

    @Override
    public void crearMascota(Mascota mascota) {
        em.persist(mascota);
    }

    @Override
    public void modificarMascota(Mascota mascota) {
        em.merge(mascota);
    }
    
}
