/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import dominio.Almacen;
import dominio.AlmacenHasPresentacion;
import dominio.Promocion;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import servicio.AlmacenServicio;
import servicio.PromocionServicio;

/**
 *
 * @author m_fer
 */
@Named("PromocionBean")
@RequestScoped
public class PromocionBean {
    
    @Inject
    private PromocionServicio promocionServicio;
    @Inject
    private AlmacenServicio almacenServicio;
    Logger log = LogManager.getRootLogger();
    
    //Varibales-Atributos de clase
    private List<Promocion> listadoPromociones;
    private List<AlmacenHasPresentacion> listadoProductos;
    private Almacen productoSeleccionado;
    private Promocion promocion;
    private Promocion promocionSeleccionada;
    private List<Almacen> listadoAlmacen;
    
    public PromocionBean(){}
    
    @PostConstruct
    public void init(){
        listadoPromociones = promocionServicio.traeTodo();
        listadoProductos = almacenServicio.listarProductos();
        this.promocion = new Promocion();
        this.promocionSeleccionada = new Promocion();
        listadoAlmacen = almacenServicio.listarAlmacer();
        productoSeleccionado = new Almacen();
    }
    
    
    //Métodos y funciones
    
    public void registrarPromocion(){
        promocion.setAlmacenidProducto(productoSeleccionado);
        promocionServicio.insertar(promocion);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro exitoso", "Se ha registrado promoción satisfactoriamente en base de datos"));
        listadoPromociones = promocionServicio.traeTodo();
    }
    
    public void editListener (RowEditEvent event){
        this.promocionSeleccionada = (Promocion) event.getObject();
        promocionServicio.modificar(promocionSeleccionada);
    }
    
    public void onRowSelectPromo(SelectEvent event) {
        this.productoSeleccionado = (Almacen) event.getObject();
    }

    public void onRowUnselectPromo(UnselectEvent event) {
        this.productoSeleccionado = new Almacen();
    }
        
    //Getters y Setters

    public List<Almacen> getListadoAlmacen() {
        return listadoAlmacen;
    }

    public void setListadoAlmacen(List<Almacen> listadoAlmacen) {
        this.listadoAlmacen = listadoAlmacen;
    }
    

    public List<Promocion> getListadoPromociones() {
        return listadoPromociones;
    }

    public void setListadoPromociones(List<Promocion> listadoPromociones) {
        this.listadoPromociones = listadoPromociones;
    }

    public Promocion getPromocion() {
        return promocion;
    }

    public void setPromocion(Promocion promocion) {
        this.promocion = promocion;
    }

    public Promocion getPromocionSeleccionada() {
        return promocionSeleccionada;
    }

    public void setPromocionSeleccionada(Promocion PromocionSeleccionada) {
        this.promocionSeleccionada = PromocionSeleccionada;
    }

    public List<AlmacenHasPresentacion> getListadoProductos() {
        return listadoProductos;
    }

    public void setListadoProductos(List<AlmacenHasPresentacion> listadoProductos) {
        this.listadoProductos = listadoProductos;
    }

    public Almacen getProductoSeleccionado() {
        return productoSeleccionado;
    }

    public void setProductoSeleccionado(Almacen productoSeleccionado) {
        this.productoSeleccionado = productoSeleccionado;
    }
    
}
