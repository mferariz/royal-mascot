/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import dominio.Proveedor;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.PrimeFaces;
import org.primefaces.event.RowEditEvent;
import servicio.ProveedorServicio;

/**
 *
 * @author m_fer
 */

@Named("ProveedorBean")
@RequestScoped
public class ProveedorBean {
    
    @Inject
    private ProveedorServicio proveedorServicio;
    Logger log = LogManager.getRootLogger();
    
    public ProveedorBean() {}
    
    //Varibales-Atributos de clase
    private List<Proveedor> listadoProveedores;
    private Proveedor proveedor;
    private Proveedor proveedorSeleccionado;
    private boolean rend;
    
    @PostConstruct
    public void init(){
        listadoProveedores = proveedorServicio.traeTodo();
        proveedor = new Proveedor();
        proveedorSeleccionado = new Proveedor();
        rend = true;
    }
    
    //Getters and setters

    public List<Proveedor> getListadoProveedores() {
        return listadoProveedores;
    }

    public void setListadoProveedores(List<Proveedor> listadoProveedores) {
        this.listadoProveedores = listadoProveedores;
    }

    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    public Proveedor getProveedorSeleccionada() {
        return proveedorSeleccionado;
    }

    public void setProveedorSeleccionada(Proveedor proveedorSeleccionada) {
        this.proveedorSeleccionado = proveedorSeleccionada;
    }
    
    public boolean isRend() {
        return rend;
    }

    public void setRend(boolean rend) {
        this.rend = rend;
    }

    //Métodos y funciones
    
    public void registrarProveedor(){
        proveedor.setStatus("A");
        proveedorServicio.insertar(proveedor);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro exitoso", "Se ha registrado el proveedor satisfactoriamente en base de datos"));
        listadoProveedores = proveedorServicio.traeTodo();
    }
    
    public void editListener (RowEditEvent event){
        proveedorSeleccionado = (Proveedor) event.getObject();
        proveedorServicio.modificar(proveedorSeleccionado);
    }
    
    public void buscarVPorId() {
        try {
            this.proveedor = proveedorServicio.traePorId(proveedor.getIdProveedor());
            this.rend = false;
            this.proveedor.setIdProveedor(null);
            PrimeFaces.current().executeScript("document.getElemenById('frmbuscar').reset();");
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Fuera de Rango", "El proveedor no existe"));
        }
    }
    
    
}
