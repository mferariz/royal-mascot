/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import dominio.Almacen;
import dominio.AlmacenHasPresentacion;
import dominio.Presentacion;
import dominio.Proveedor;
import dominio.Tipoproducto;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import servicio.AlmacenServicio;

/**
 *
 * @author MarkII
 */
@Named("AlmacenBean")
@RequestScoped
public class AlmacenBeanCtrl {
    
    @Inject
    private AlmacenServicio almacenServicio;

    //Varibales-Atributos de clase
    private List<AlmacenHasPresentacion> listadoProductos;
    private List<Proveedor> listadoProveedores;
    private List<Tipoproducto> listadoTiposProd;
    private List<Presentacion> listadoPresentacion;
    private AlmacenHasPresentacion productoModificar;

    private AlmacenHasPresentacion almacenHasPresentacion;
    private Tipoproducto tipoProducto;
    private Almacen almacen;
    private Presentacion presentacion;
    private Proveedor proveedor;

    private Proveedor provSel = new Proveedor();
    private Tipoproducto tipoProdSel = new Tipoproducto();
    private Presentacion presenSel = new Presentacion();
    

    //constructores
    public AlmacenBeanCtrl() {
    }

    @PostConstruct
    public void init() {
        listadoTiposProd = almacenServicio.listarTipoProducto();
        listadoProveedores = almacenServicio.listarProveedores();
        listadoPresentacion = almacenServicio.listarPresentacion();
        listadoProductos = almacenServicio.listarProductos();
        this.almacenHasPresentacion = new AlmacenHasPresentacion();
        this.tipoProducto = new Tipoproducto();
        this.almacen = new Almacen();
        this.presentacion = new Presentacion();
        this.proveedor = new Proveedor();
        this.productoModificar = new AlmacenHasPresentacion();
    }

//Métodos y funciones
    
    
    public void registrarProducto() {
        almacen.setTipoProductoidProducto(tipoProdSel);
        almacen.setProveedoridProveedor(provSel);
        almacenHasPresentacion.setAlmacenidProducto(almacen);
        almacenHasPresentacion.setPresentacionidPresentacion(presenSel);
        almacenServicio.agregarproducto(almacenHasPresentacion);
        this.listadoProductos = almacenServicio.listarProductos();
    }
    
    public void editListener (RowEditEvent event){
        this.productoModificar = (AlmacenHasPresentacion) event.getObject();
        almacenServicio.modificarProducto(productoModificar);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Modificación de registro", "Se ha registrado satisfactoriamente el producto"));
    }

    
    public void onRowSelectProv(SelectEvent event) {
        this.provSel = (Proveedor) event.getObject();
    }

    public void onRowUnselectProv(UnselectEvent event) {
        this.provSel = null;
    }
    
    public void onRowSelectTipo(SelectEvent event) {
        this.tipoProdSel = (Tipoproducto) event.getObject();
    }

    public void onRowUnselectTipo(UnselectEvent event) {
        this.tipoProdSel = null;
    }
    
    public void onRowSelectPresentacion(SelectEvent event) {
        this.presenSel = (Presentacion) event.getObject();
    }

    public void onRowUnselectPresentacion(UnselectEvent event) {
        this.presenSel = null;
    }
    

    //Getters y Setters

    public AlmacenHasPresentacion getProductoModificar() {
        return productoModificar;
    }

    public void setProductoModificar(AlmacenHasPresentacion productoModificar) {
        this.productoModificar = productoModificar;
    }
    
    
    
    public Proveedor getProvSel() {
        return provSel;
    }

    public void setProvSel(Proveedor provSel) {
        this.provSel = provSel;
    }

    public Tipoproducto getTipoProdSel() {
        return tipoProdSel;
    }

    public void setTipoProdSel(Tipoproducto tipoProdSel) {
        this.tipoProdSel = tipoProdSel;
    }

    public Presentacion getPresenSel() {
        return presenSel;
    }

    public void setPresenSel(Presentacion presenSel) {
        this.presenSel = presenSel;
    }

    public List<AlmacenHasPresentacion> getListadoProductos() {
        return listadoProductos;
    }

    public void setListadoProductos(List<AlmacenHasPresentacion> listadoProductos) {
        this.listadoProductos = listadoProductos;
    }

    public Tipoproducto getTipoProducto() {
        return tipoProducto;
    }

    public void setTipoProducto(Tipoproducto tipoProducto) {
        this.tipoProducto = tipoProducto;
    }

    public Almacen getAlmacen() {
        return almacen;
    }

    public void setAlmacen(Almacen almacen) {
        this.almacen = almacen;
    }

    public Presentacion getPresentacion() {
        return presentacion;
    }

    public void setPresentacion(Presentacion presentacion) {
        this.presentacion = presentacion;
    }

    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    public List<Proveedor> getListadoProveedores() {
        return listadoProveedores;
    }

    public void setListadoProveedores(List<Proveedor> listadoProveedores) {
        this.listadoProveedores = listadoProveedores;
    }

    public List<Tipoproducto> getListadoTiposProd() {
        return listadoTiposProd;
    }

    public void setListadoTiposProd(List<Tipoproducto> listadoTiposProd) {
        this.listadoTiposProd = listadoTiposProd;
    }

    public List<Presentacion> getListadoPresentacion() {
        return listadoPresentacion;
    }

    public void setListadoPresentacion(List<Presentacion> listadoPresentacion) {
        this.listadoPresentacion = listadoPresentacion;
    }

    public AlmacenHasPresentacion getAlmacenHasPresentacion() {
        return almacenHasPresentacion;
    }

    public void setAlmacenHasPresentacion(AlmacenHasPresentacion almacenHasPresentacion) {
        this.almacenHasPresentacion = almacenHasPresentacion;
    }
}
