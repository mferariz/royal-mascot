/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import dominio.Administrador;
import dominio.Persona;
import dominio.Vendedor;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import servicio.loginServicio;

/**
 *
 * @author MarkII
 */
@Named("loginBean")
@RequestScoped
public class loginBean {
    @Inject
    private loginServicio loginServicio;
    
    private Administrador usuAdmin;
    private Vendedor usuVende;
    private Persona persona;
    private String tipoUsuario;
    private String contrasena;
    
    @PostConstruct
    public void init(){
        this.usuAdmin = new Administrador();
        this.usuVende = new Vendedor();
        this.persona = new Persona();
    }

    public String iniciarSesion(){
        String redirect = null;
        try {
            if (this.tipoUsuario.equals("administrador")) {
                usuAdmin.setPersonaidPersona(persona);
                usuAdmin.setContrasenaA(contrasena);
                loginServicio.usuarioAdmin(usuAdmin);
                redirect = "index";
                return redirect;
            }else if (this.tipoUsuario.equals("vendedor")) {
                usuVende.setPersonaidPersona(persona);
                usuVende.setContrasenaV(contrasena);
                loginServicio.usuarioVendedor(usuVende);
                redirect = "index";
                return redirect;
            }
        } catch (Exception e) {
            System.out.println(e);
            //mensaje jsf
            
        }
        return redirect;
    }
    
    
    //Funciones metodos de controlador
    
    
    public void mostrarLogo(){
        FacesContext.getCurrentInstance().getExternalContext();
    }
    
    //geters y setter

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }
    
    public Administrador getUsuAdmin() {
        return usuAdmin;
    }

    public void setUsuAdmin(Administrador usuAdmin) {
        this.usuAdmin = usuAdmin;
    }

    public Vendedor getUsuVende() {
        return usuVende;
    }

    public void setUsuVende(Vendedor usuVende) {
        this.usuVende = usuVende;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public String getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(String tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }
    
     
    
    
   

}
