/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import dominio.Persona;
import servicio.PersonaServicio;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author MarkII
 * 
 */

@Named("personaBean")
@RequestScoped
public class PersonaBean {
    
    
    @Inject
    private PersonaServicio personaServicio;
    
    private Persona personaSeleccionada;
    List<Persona> personas;
    
    public PersonaBean(){
        
    }
    
    @PostConstruct
    public void inicializar(){
        personas = personaServicio.listarPersonas();
        
        this.personaSeleccionada = new Persona();
    }
    
    public void editListener (RowEditEvent event){
        Persona persona = (Persona) event.getObject();
        personaServicio.modificarPersona(persona);
    }

    public Persona getPersonaSeleccionada() {
        return personaSeleccionada;
    }

    public void setPersonaSeleccionada(Persona personaSeleccionada) {
        this.personaSeleccionada = personaSeleccionada;
    }

    public List<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(List<Persona> personas) {
        this.personas = personas;
    }
    
    public void agregarPersona(){
        this.personaServicio.crearPersona(personaSeleccionada);
        this.personas.add(personaSeleccionada);
        this.personaSeleccionada = null;
    }
    
    
    public void reiniciarPersonaSeleccionada(){
        this.personaSeleccionada = new Persona();
    }
    
}
