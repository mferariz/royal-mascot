/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import dominio.Administrador;
import dominio.Gasto;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.PrimeFaces;
import org.primefaces.event.RowEditEvent;
import servicio.GastoServicio;

/**
 *
 * @author luis-
 */
@Named("GastoBean")
@RequestScoped
public class GastoBean {

    @Inject
    private GastoServicio gastoServicio;

    //Variables-Atributos de clase
    private static List<Gasto> listadoGastos;
    private Gasto gasto;
    private Administrador admin;
    private Gasto gastoSeleccionado;
    private Date fechaRes, fechaI, fechaF;
    private String contP;
    private String contV;
    private Double monto;
    private static Double suma;
    private static Boolean tipoG;
    private static String img;

    public GastoBean() {
    }

    @PostConstruct
    public void init() {
        this.admin = new Administrador();
        this.gastoSeleccionado = new Gasto();
        this.gasto = new Gasto();
        this.fechaRes = new Date();
        this.monto = null;
    }

    //Métodos y Funciones
    public void agregarGasto() {
        try {
            establecerDatos();
            gastoServicio.insertarGasto(gasto);
            limpiarCampos();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro exitoso", "Se ha registrado el gasto"));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se registro el gasto"));
            limpiarCampos();
        }

    }

    public void bucarXTipo() {
        listadoGastos = gastoServicio.encontrarGT(gasto);
        sumaGastos();
        xTipo();
    }

    public void bucarXFiltroV() {
        listadoGastos = gastoServicio.encontrarGVF(gasto);
        if (listadoGastos.isEmpty()) {
            this.gasto.setTipo("Variable");
            bucarXTipo();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Ups", "Parece que no hay gastos para ese día"));
        } else {
            sumaGastos();
        }
    }

    public void bucarXFiltroP() {
        if (fechaI.compareTo(fechaF) <= 0) {
            listadoGastos = gastoServicio.encontrarGPF(fechaI, fechaF);
            if (listadoGastos.isEmpty()) {
                this.gasto.setTipo("Permanente");
                bucarXTipo();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Ups", "Parece que no hay gastos para ese día"));
            } else {
                sumaGastos();
            }
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Ups", "Coloca las fechas de manera lógica"));
        }
    }

    public void sumaGastos() {
        suma = 0.0;
        for (Gasto g : listadoGastos) {
            suma += g.getMonto();
        }
    }

    public void xTipo() {
        if(gasto.getTipo().equals("Permanente")){
            tipoG = true;
            img = "perrito2.png";
        }else{
            tipoG = false;
            img = "catV.png";
        }
                
    }

    public void editGasto(RowEditEvent event) {
        try {
            this.gastoSeleccionado = (Gasto) event.getObject();
            gastoServicio.modificarGasto(gastoSeleccionado);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Hecho", "Se ha modificado el gasto"));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Algo salio mal", "Seguro de que llenaste todos los campos?"));
        }
    }

    public void establecerDatos() {
        gasto.setMonto(monto);
        admin.setIdAdministrador(1);
        gasto.setAdministradoridAdministrador(admin);
        establecerContenido();
        gasto.setFecha(fechaRes);
    }    
    
    public void seleccionTipo() {
        if ("Permanente".equals(this.gasto.getTipo())) {
            PrimeFaces.current().executeScript("document.getElementById('frmDialogo:pgConV').style.display = 'none';");
            PrimeFaces.current().executeScript("document.getElementById('frmDialogo:pgConP').style.display = 'block';");
        } else if ("Variable".equals(this.gasto.getTipo())) {
            PrimeFaces.current().executeScript("document.getElementById('frmDialogo:pgConV').style.display = 'block';");
            PrimeFaces.current().executeScript("document.getElementById('frmDialogo:pgConP').style.display = 'none';");
        }
        PrimeFaces.current().executeScript("document.getElementById('frmDialogo:pgMonto').style.display = 'block';");
    }

    public void establecerContenido() {
        if ("Permanente".equals(this.gasto.getTipo())) {
            gasto.setConcepto(contP);
        } else if ("Variable".equals(this.gasto.getTipo())) {
            gasto.setConcepto(contV);
        }
    }

    public void limpiarCampos() {
        this.gasto = new Gasto();
        monto = null;
        contP = null;
        contV = null;
        PrimeFaces.current().executeScript("document.getElementByID('frmDialogo').reset();");
    }

    //Setters y Getters
    public List<Gasto> getListadoGastos() {
        return listadoGastos;
    }

    public void setListadoGastos(List<Gasto> listadoGastos) {
        GastoBean.listadoGastos = listadoGastos;
    }

    public Gasto getGasto() {
        return gasto;
    }

    public void setGasto(Gasto gasto) {
        this.gasto = gasto;
    }

    public Gasto getGastoSeleccionado() {
        return gastoSeleccionado;
    }

    public void setGastoSeleccionado(Gasto gastoSeleccionado) {
        this.gastoSeleccionado = gastoSeleccionado;
    }

    public Date getFechaRes() {
        return fechaRes;
    }

    public void setFechaRes(Date fechaRes) {
        this.fechaRes = fechaRes;
    }

    public Administrador getAdmin() {
        return admin;
    }

    public void setAdmin(Administrador admin) {
        this.admin = admin;
    }

    public String getContP() {
        return contP;
    }

    public void setContP(String contP) {
        this.contP = contP;
    }

    public String getContV() {
        return contV;
    }

    public void setContV(String contV) {
        this.contV = contV;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    public Double getSuma() {
        return suma;
    }

    public void setSuma(Double suma) {
        GastoBean.suma = suma;
    }

    public Boolean getTipoG() {
        return tipoG;
    }

    public void setTipoG(Boolean tipoG) {
        GastoBean.tipoG = tipoG;
    }

    public Date getFechaI() {
        return fechaI;
    }

    public void setFechaI(Date fechaI) {
        this.fechaI = fechaI;
    }

    public Date getFechaF() {
        return fechaF;
    }

    public void setFechaF(Date fechaF) {
        this.fechaF = fechaF;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        GastoBean.img = img;
    }
    
    
}
