/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import dominio.Persona;
import dominio.Vendedor;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.PrimeFaces;
import org.primefaces.event.RowEditEvent;
import servicio.VendedorServicio;

/**
 *
 * @author MarkII
 */
@Named("VendedorBean")
@RequestScoped
public class VendedorBean {

    @Inject
    private VendedorServicio vendedorServicio;

    //Varibales-Atributos de clase
    private List<Vendedor> listadoVendedores;
    private Vendedor vendedor;
    private Persona persona;
    private Vendedor vendedorSeleccionado;
    private Date fechaRes;
    private boolean rend;

    public VendedorBean() {
    }

    @PostConstruct
    public void init() {
        listadoVendedores = vendedorServicio.listarVendedores();
        this.vendedor = new Vendedor();
        this.persona = new Persona();
        this.vendedorSeleccionado = new Vendedor();
        this.fechaRes = new Date();
        this.rend = true;
    }

    //Métodos y funciones
    public void registrarVendedor() {
        try {
            establecerDatos();
            vendedorServicio.crearVendedor(vendedor);
            this.listadoVendedores.add(vendedor);
            limpiarCampos();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro exitoso", "Se ha registrado al trabajador satisfactoriamente en la base de datos"));
            listadoVendedores = vendedorServicio.listarVendedores();
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Ha ocurrido un error", "No se ha podido  registrar al trabajador"));
        }

    }

    public void buscarVPorId() {
        try {
            this.vendedor = vendedorServicio.buscarPorId(vendedor);
            this.persona = vendedor.getPersonaidPersona();
            rend = false;
            vendedor.setIdVendedor(null);
            PrimeFaces.current().executeScript("document.getElemenById('frmbuscar').reset();");
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Fuera de Rango", "El trabajador no existe"));
        }
    }

    public void editListener(RowEditEvent event) {
        try {
            this.vendedorSeleccionado = (Vendedor) event.getObject();
            vendedorServicio.modificarVendedor(vendedorSeleccionado);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Hecho", "Se ha modificado al trabajador"));
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Algo salio mal", "No se ha modificado al trabajador"));
        }
    }

    public void limpiarCampos() {        //limpia campos del formulario agregar vendedor
        this.vendedor = new Vendedor();
        this.persona = new Persona();
        PrimeFaces.current().executeScript("document.getElementById('frmAgregarVendedor').reset();");
    }

    public void establecerDatos() {
        persona.setFechaRegistro(fechaRes);
        persona.setStatus("1");
        vendedor.setPersonaidPersona(persona);
    }

    //Getters y Setters
    public List<Vendedor> getListadoVendedores() {
        return listadoVendedores;
    }

    public void setListadoVendedores(List<Vendedor> listadoVendedores) {
        this.listadoVendedores = listadoVendedores;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Vendedor getVendedorSeleccionado() {
        return vendedorSeleccionado;
    }

    public void setVendedorSeleccionado(Vendedor vendedorSeleccionado) {
        this.vendedorSeleccionado = vendedorSeleccionado;
    }

    public Date getFechaRes() {
        return fechaRes;
    }

    public void setFechaRes(Date fechaRes) {
        this.fechaRes = fechaRes;
    }

    public boolean isRend() {
        return rend;
    }

    public void setRend(boolean rend) {
        this.rend = rend;
    }

}
