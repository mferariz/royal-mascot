/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import dominio.Cliente;
import dominio.Mascota;
import dominio.Persona;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import servicio.clienteMascotaServicio;

/**
 *
 * @author MarkII
 */
@Named("clienteMascotaBean")
@RequestScoped
public class clienteMascotaBean {
    
    @Inject
    private clienteMascotaServicio cmServicio;
    
    //VARIABLES
    private List<Cliente> listadoClientes;
    private List<Mascota> listadoMascotas;
    private Cliente cliente;
    private Cliente clienteModificar;
    private Mascota mascota;
    private Mascota mascotaModificar;
    private Cliente clienteSel;
    private Persona persona;
    
    //CONSTRUCTORES
    public clienteMascotaBean(){}
    
    @PostConstruct
    public void init(){
        listadoClientes = cmServicio.listadoClientes();
        listadoMascotas = cmServicio.listadoMascotas();
        cliente = new Cliente();//crear
        mascota = new Mascota();//crear mascota
        mascotaModificar = new Mascota();//modificar mascota
        clienteSel = new Cliente();//asignar crear mascota
        clienteModificar = new Cliente();//modificar cliente
        persona = new Persona();
    }
    
    //METODOS - FUNCIONES
    public void agregarCliente(){
        cliente.setPersonaidPersona(persona);
        cmServicio.crearCliente(cliente);
        listadoClientes = cmServicio.listadoClientes();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Creación de registro", "Se ha creado el registro"));
    }
    
    public void agregarMascota(){
        mascota.setClienteidCliente(clienteSel);
        cmServicio.crearMascota(mascota);
        listadoMascotas = cmServicio.listadoMascotas();
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Creación de registro", "Se ha creado el registro"));
    }
    
    public void onRowSelectCliente(SelectEvent event){
        this.clienteSel = (Cliente) event.getObject();
    }
    
    public void editListenerCliente (RowEditEvent event){
        this.clienteModificar = (Cliente) event.getObject();
        cmServicio.modificarCliente(clienteModificar);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Modificación de registro", "Se ha modificado el registro"));
    }
    public void editListenerMascota (RowEditEvent event){
        this.mascotaModificar = (Mascota) event.getObject();
        cmServicio.modificarMascota(mascotaModificar);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Modificación de registro", "Se ha modificado el registro"));
    }
    
    //GETTER - SETTER

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }
    
    

    public Cliente getClienteModificar() {
        return clienteModificar;
    }

    public void setClienteModificar(Cliente clienteModificar) {
        this.clienteModificar = clienteModificar;
    }
    

    public List<Cliente> getListadoClientes() {
        return listadoClientes;
    }

    public void setListadoClientes(List<Cliente> listadoClientes) {
        this.listadoClientes = listadoClientes;
    }

    public List<Mascota> getListadoMascotas() {
        return listadoMascotas;
    }

    public void setListadoMascotas(List<Mascota> listadoMascotas) {
        this.listadoMascotas = listadoMascotas;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Mascota getMascota() {
        return mascota;
    }

    public void setMascota(Mascota mascota) {
        this.mascota = mascota;
    }

    public Mascota getMascotaModificar() {
        return mascotaModificar;
    }

    public void setMascotaModificar(Mascota mascotaSel) {
        this.mascotaModificar = mascotaSel;
    }

    public Cliente getClienteSel() {
        return clienteSel;
    }

    public void setClienteSel(Cliente clienteSel) {
        this.clienteSel = clienteSel;
    }
    
}
