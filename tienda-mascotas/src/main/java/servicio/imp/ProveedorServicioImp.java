/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicio.imp;

import datos.ProveedorDao;
import dominio.Proveedor;
import java.util.List;
import javax.inject.Inject;
import servicio.ProveedorServicio;

/**
 *
 * @author m_fer
 */
public class ProveedorServicioImp implements ProveedorServicio{
    
    @Inject
    private ProveedorDao proveedorDao;

    @Override
    public List<Proveedor> traeTodo() {
        return proveedorDao.traeTodo();
    }

    @Override
    public Proveedor traePorId(Integer id) {
        return proveedorDao.traePorId(id);
    }

    @Override
    public void insertar(Proveedor o) {
        proveedorDao.insertar(o);
    }

    @Override
    public void modificar(Proveedor o) {
        proveedorDao.modificar(o);
    }

    @Override
    public void eliminar(Proveedor o) {
        proveedorDao.eliminar(o);
    }
    
}
