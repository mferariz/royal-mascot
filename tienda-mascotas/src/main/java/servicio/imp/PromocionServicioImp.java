/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicio.imp;

import datos.PromocionDao;
import dominio.Promocion;
import java.util.List;
import javax.inject.Inject;
import servicio.PromocionServicio;

/**
 *
 * @author m_fer
 */
public class PromocionServicioImp implements PromocionServicio{

    @Inject
    private PromocionDao promocionDao;
    
    @Override
    public List<Promocion> traeTodo() {
        return promocionDao.traeTodo();
    }

    @Override
    public Promocion traePorId(Integer id) {
        return promocionDao.traePorId(id);
    }

    @Override
    public void insertar(Promocion o) {
        promocionDao.insertar(o);
    }

    @Override
    public void modificar(Promocion o) {
        promocionDao.modificar(o);
    }

    @Override
    public void eliminar(Promocion o) {
        promocionDao.eliminar(o);
    }
    
}
