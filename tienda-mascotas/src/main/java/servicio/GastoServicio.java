/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicio;

import dominio.Gasto;
import java.util.Date;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author luis-
 */
@Local
public interface GastoServicio {
    public List<Gasto> encontrarGVF(Gasto gasto);
    public List<Gasto> encontrarGPF(Date fechaI, Date fechaF);
    public List<Gasto> encontrarGT(Gasto gasto);
    public void modificarGasto(Gasto gasto);
    public void insertarGasto(Gasto gasto);
    public void eliminarGasto(Gasto gasto);      
}
