/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicio;

import datos.VendedorDao;
import dominio.Vendedor;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author MarkII
 */
@Stateless
public class VendedorServicioImp implements VendedorServicio{
    
    @Inject
    private VendedorDao vendedorDao;

    @Override
    public List<Vendedor> listarVendedores() {
        return vendedorDao.listarVendedores();
    }

    @Override
    public Vendedor buscarPorId(Vendedor vendedor) {
        return vendedorDao.buscarPorId(vendedor);
    }

    @Override
    public void crearVendedor(Vendedor vendedor) {
        vendedorDao.crearVendedor(vendedor);
    }

    @Override
    public void modificarVendedor(Vendedor vendedor) {
        vendedorDao.modificarVendedor(vendedor);
    }
    
}
