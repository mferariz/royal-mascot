/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicio;

import dominio.Administrador;
import dominio.Vendedor;
import javax.ejb.Local;

/**
 *
 * @author MarkII
 */
@Local
public interface loginServicio {
    public Administrador usuarioAdmin(Administrador admin);
    public Vendedor usuarioVendedor(Vendedor vend);
}
