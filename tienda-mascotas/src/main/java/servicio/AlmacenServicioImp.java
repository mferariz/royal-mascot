/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicio;

import datos.AlmacenDao;
import dominio.Almacen;
import dominio.AlmacenHasPresentacion;
import dominio.Cliente;
import dominio.Mascota;
import dominio.Presentacion;
import dominio.Proveedor;
import dominio.Tipoproducto;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author MarkII
 */
@Stateless
public class AlmacenServicioImp implements AlmacenServicio{
    @Inject
    private AlmacenDao almacenDao;

    @Override
    public List<AlmacenHasPresentacion> listarProductos() {
        return almacenDao.listarProductos();
    }
    
    @Override
    public void agregarproducto(AlmacenHasPresentacion almacenHasPresentacion){
        almacenDao.agregarproducto(almacenHasPresentacion);
    }
    
    @Override
    public void modificarProducto(AlmacenHasPresentacion productoModificar){
        almacenDao.modificarProducto(productoModificar);
    }
    
    @Override
    public List<Proveedor> listarProveedores(){
        return almacenDao.listarProveedores();
    }
    
    @Override
    public List<Tipoproducto> listarTipoProducto(){
        return almacenDao.listarTipoProducto();
    }
    
    @Override
    public List<Presentacion> listarPresentacion(){
        return almacenDao.listarPresentacion();
    }
    
    @Override
    public List<Almacen> listarAlmacer(){
        return almacenDao.listarAlmacer();
    }
    
}
