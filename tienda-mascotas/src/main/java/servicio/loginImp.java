/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicio;

import datos.loginDao;
import dominio.Administrador;
import dominio.Vendedor;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author MarkII
 */
@Stateless
public class loginImp implements loginServicio{
    
    @Inject
    private loginDao loginDao;

    @Override
    public Administrador usuarioAdmin(Administrador admin) {
        return loginDao.accederAdministrador(admin);
    }

    @Override
    public Vendedor usuarioVendedor(Vendedor vend) {
        return loginDao.accederVendedor(vend);
    }

    

    
    
}
