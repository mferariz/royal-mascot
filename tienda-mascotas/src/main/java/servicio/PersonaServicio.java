/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicio;

import dominio.Persona;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author MarkII
 */
@Local
public interface PersonaServicio {
    public List<Persona> listarPersonas();
    public Persona encontrarPersonaPorId(Persona persona);
    public Persona encontrarPersonaPorCorreo(Persona persona);
    public Persona encontrarPersonaPorApellidos(Persona persona);
    public void crearPersona(Persona persona);
    public void modificarPersona(Persona persona);
    
}
