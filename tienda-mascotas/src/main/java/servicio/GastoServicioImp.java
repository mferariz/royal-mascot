/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicio;

import datos.GastoDao;
import dominio.Gasto;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author luis-
 */
@Stateless
public class GastoServicioImp implements GastoServicio{

    @Inject 
    private GastoDao gastoDao;
    
    @Override
    public List<Gasto> encontrarGVF(Gasto gasto) {
        return gastoDao.encontrarGVF(gasto);
    }
    
    @Override
    public List<Gasto> encontrarGPF(Date fechaI, Date fechaF){
        return gastoDao.encontrarGPF(fechaI,fechaF);
    }

    @Override
    public List<Gasto> encontrarGT(Gasto gasto) {
        return gastoDao.encontrarGT(gasto);
    }
    
    @Override
    public void modificarGasto(Gasto gasto) {
        gastoDao.modificarGasto(gasto);
    }

    @Override
    public void insertarGasto(Gasto gasto) {
        gastoDao.insertarGasto(gasto);
    }

    @Override
    public void eliminarGasto(Gasto gasto) {
        gastoDao.eliminarGasto(gasto);
    }    
    
}
