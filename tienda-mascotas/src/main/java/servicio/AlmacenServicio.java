/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicio;

import dominio.Almacen;
import dominio.AlmacenHasPresentacion;
import dominio.Cliente;
import dominio.Mascota;
import dominio.Presentacion;
import dominio.Proveedor;
import dominio.Tipoproducto;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author MarkII
 */
@Local
public interface AlmacenServicio {
    public List<AlmacenHasPresentacion> listarProductos();
    public void agregarproducto(AlmacenHasPresentacion almacenHasPresentacion);
    public void modificarProducto(AlmacenHasPresentacion productoModificar);
    public List<Proveedor> listarProveedores();
    public List<Tipoproducto> listarTipoProducto();
    public List<Presentacion> listarPresentacion();
    public List<Almacen> listarAlmacer();
}
