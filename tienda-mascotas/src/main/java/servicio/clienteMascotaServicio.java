/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicio;

import dominio.Cliente;
import dominio.Mascota;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author MarkII
 */
@Local
public interface clienteMascotaServicio {
    //Cliente
    public List<Cliente> listadoClientes();
    public void crearCliente(Cliente cliente);
    public void modificarCliente(Cliente cliente);
    //Mascota
    public List<Mascota> listadoMascotas();
    public void crearMascota(Mascota mascota);
    public void modificarMascota(Mascota mascota);
}
