/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicio;

import datos.PersonaDao;
import dominio.Persona;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author MarkII
 */
@Stateless
public class PersonaServicioImp implements PersonaServicio{

    @Inject
    private PersonaDao personaDao;
    
    @Override
    public List<Persona> listarPersonas() {
        return personaDao.enconrtarTodasPersonas();
    }

    @Override
    public Persona encontrarPersonaPorId(Persona persona) {
        return personaDao.encontrarPersonaPorId(persona);
    }

    @Override
    public Persona encontrarPersonaPorCorreo(Persona persona) {
        return personaDao.encontrarPersonaPorCorreo(persona);
    }
    
     @Override
    public Persona encontrarPersonaPorApellidos(Persona persona) {
        return personaDao.encontrarPersonaPorApellidos(persona);
    }

    @Override
    public void crearPersona(Persona persona) {
        personaDao.insertarPersona(persona);
    }

    @Override
    public void modificarPersona(Persona persona) {
        personaDao.modificarPersona(persona);
    }

   
    
}
