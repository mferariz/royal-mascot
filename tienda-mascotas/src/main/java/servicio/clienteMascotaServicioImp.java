/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicio;

import datos.clienteMascotaDao;
import dominio.Cliente;
import dominio.Mascota;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author MarkII
 */
@Stateless
public class clienteMascotaServicioImp implements clienteMascotaServicio{

    @Inject
    private clienteMascotaDao cmDao;
    
    @Override
    public List<Cliente> listadoClientes() {
        return cmDao.listadoClientes();
    }

    @Override
    public void crearCliente(Cliente cliente) {
        cmDao.crearCliente(cliente);
    }

    @Override
    public void modificarCliente(Cliente cliente) {
        cmDao.modificarCliente(cliente);
    }

    @Override
    public List<Mascota> listadoMascotas() {
        return cmDao.listadoMascotas();
    }

    @Override
    public void crearMascota(Mascota mascota) {
        cmDao.crearMascota(mascota);
    }

    @Override
    public void modificarMascota(Mascota mascota) {
        cmDao.modificarMascota(mascota);
    }
    
}
